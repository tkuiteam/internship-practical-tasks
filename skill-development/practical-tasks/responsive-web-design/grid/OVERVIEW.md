# Responsive Grid

Implement fluid grid layout using CSS3 technics, without including existing Grid Systems.

![responsive grid mockup](img/responsive-grid.png "Responsive Grid Mockup" )

### Environment
There is no requirements about environment, you can use two files HTML and CSS or you can setup build tools, to be able to use SASS or other CSS pre-processors. Make sure you are not using any Grid Systems like (Bootstrap, Foundation or Susy), everything needs to be written using clean CSS or SASS.

### Acceptance Criteria
- Grid should have 12 columns similar to Bootstrap or Foundation. Class names can be the same as well.
- Apply @media queries to update these measurements to best arrange grid columns on any viewport.
- Mobile first approach needs to be used. Please refer to courses to know more about MFA.

### Demo project
**General**
- All product needs to be separated by line.
- Product cell content centered

**Mobile**
- Two columns or one hero product at the top or bottom.

**Desktop and Tablet**
- Four columns or 2/4 hero product left or right positioned.