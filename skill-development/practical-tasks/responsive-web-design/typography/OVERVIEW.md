# Responsive Typography

## Requirements

The idea of this task is to get you acquainted with responsive typography and various techniques for text formatting/styling. You will have a chance to practice on a real design which is meant to be pixel-perfect, backwards compatible and accessible.

Images would have to adapt to pixel density, device orientation, supported image formats and, of course, screen dimension. There are also places for art direction where mobile/desktop images are the same, but different in their perspective and aspect ratios.

Text would have to become smaller/larger depending on the device (e.g. body text on mobile would have to be 12px, but on desktop - 16px) for readability purposes. On larger screens it would have to incrementally become bigger with the help of viewport units. Margins, paddings, line-heights would be different depending on screen size aswell.

Along with mobile-first approach, you will have to apply progressive enhancement strategy for properties that are not supported in older browsers.
Code should be organized that way so it's easy to switch blocks between each other and reuse in the future. Base styles structure should be carefully designed from the very beginning so that it's easy to change it in the future in case breakpoints or body font sizes (12px, 14px, 16px) would be changed.

## Technical requirements

### HTML
- Use schema markup for addresses;
- Use figure/figcaption for images/caption;
- Make heavy use of picture tag w/ polyfill;
- Crop images for different viewports as per design;
- Add support for *.webp image format for Chrome;
- Use meta (e.g. open graph) tags for sharing posts on social media.

### Accessibility
- Get an idea of what WCAG stands for;
- Use appropriate tags where needed (e.g. blockquote, q, cite, date, abbr etc.);
- Use aria and role attributes;
- Use fieldset, legend, label tags for forms.

### SVG
- Optimize SVG assets for performance;
- Avoid *.svg requests - inline or embed as a symbol;
- Make it possible to change element's colours through CSS.

### CSS
- Use media queries and take breakpoints from Foundation framework;
- Use media queries for print so that page is well-printed;
- Use landscape-orientation when it's appropriate;
- Use aspect-ratio for images;
- Use columns for two-column text layout;
- Use presudo-classes wherever it's appropriate;
- Use css counters for ordered lists and figure captions;
- Use image-lists for unordered lists;
- Use shapes for text wrapping;
- Use background and object-fit properties;
- Use rem, em, % units and avoid pixels for responsiveness.

## Resources

### Books
- [Typography Handbook](http://typographyhandbook.com/)
- [Thinking with Type](http://thinkingwithtype.com/)
- [Web Typography](http://webtypography.net/)

### Articles
- [Progressive Enhancement: What It Is, And How To Use It?](https://www.smashingmagazine.com/2009/04/progressive-enhancement-what-it-is-and-how-to-use-it/)
- [Getting started with schema.org using Microdata](http://schema.org/docs/gs.html)
- [The figure & figcaption elements](http://html5doctor.com/the-figure-figcaption-elements/)
- [Responsive Images](https://dev.opera.com/articles/responsive-images/)
- [Picture Element](https://www.html5rocks.com/en/tutorials/responsive/picture-element/)
- [The Open Graph protocol](http://ogp.me/)
- [Accessibility ARIA attribute](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA)
- [Using SVG](https://css-tricks.com/using-svg/)
- [SVG use with External Reference](https://css-tricks.com/svg-use-with-external-reference-take-2/)
- [How To Set Up A Print Style Sheet](https://www.smashingmagazine.com/2011/11/how-to-set-up-a-print-style-sheet/)
- [Aspect Ratio Boxes](https://css-tricks.com/aspect-ratio-boxes/)
- [Using multi-column Layouts](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Columns/Using_multi-column_layouts)
- [Pseudo classes](https://developer.mozilla.org/en-US/docs/Web/CSS/Pseudo-classes)
- [Getting Started with CSS Shapes](https://www.html5rocks.com/en/tutorials/shapes/getting-started/)
- [Masking vs. Clipping: When to Use Each](https://css-tricks.com/masking-vs-clipping-use/)
- [background-size property](https://developer.mozilla.org/en-US/docs/Web/CSS/background-size)
- [object-fit property](https://developer.mozilla.org/en-US/docs/Web/CSS/object-fit)
