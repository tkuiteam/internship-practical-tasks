# Responsive navigation

![responsive nav mockup](img/responsive-nav.png "Responsive Nav Mockup")

### Environment
Please use CSS/Sass and plain HTML with no dependencies on any responsive frameworks like Foundation/Bootstrap. If you choose to create a carousel inside the navigation, you can use slick.js. 

Browser support:
IE 10+
Safari 6+
Firefox	44
Chrome	latest version based on auto-updates

### Acceptance criteria

**General**
* Design is up to you

* Active menu item/sub-item must be stylistically differentiated from the rest.

**Desktop and Tablet**

 * Create a dropdown navigation menu
 * A set of menu item links inlined separated by space or a divider (up to chosen designs)

 * On hover a dropdown is shown with an animation of growing height from 0 to full content size

 * Content of dropdown can vary item to item, including following options:
   - multiple column sections layout, it can be different for every submenu (i.e. 4 columns of only submenu text links for one menu item, or 3 columns with text links and 1 with image, or 2 image sections centered etc)
   - sections may include submenu item links or images representing submenu links (for category representation)
   
 * Content of dropdown must be have a maximum width depending on used breakpoints. You need to choose the best option for you. (ex. max width of 750px for all breakpoints or 100% for tablet with a margin left/right and max width of 1000px for desktop etc.)

**Mobile**
 * Create an accordion navigation menu
 
 * Initially navigation will be hidden. 
   - On "open-menu" button (i.e. any custom burger) click menu will appear with a slide in animation. The direction of sliding is up to you - left to right / top to bottom etc. 
   - On "close-menu" button click (ex. cross or anything else to reflect close button) menu will hide in respective reversed animation used for its opening.
 
 * Menu will take full size of viewport (screen)
 
 * Menu items will be closed by default. You can design any element (dropdown arrow, a plus etc) to show that a menu item contains children sub-items and can be opened to access more links. On interaction (opening/closing) this element must be animated - ex. dropdown arrow rotating from down to up, plus transforming to minus etc.
 
 * On opening/closing nested menu item, container with sub-items must be displayed with a slide in animation top to bottom/bottom to top direction.
 
 * For containers with images as sub-items, you can choose one or more options to implement: 
    - display as grid of 2 columns per row
    - display as one single column
    - display as a carousel with one (more) item(s) per slide 
    - display as a sub-item list
  
