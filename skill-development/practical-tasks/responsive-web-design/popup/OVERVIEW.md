# Responsive Popup
Implement a dialog library which is reusable, configurable and easy to style.

![responsive nav mockup](img/dialog.png "Responsive Nav Mockup")

!DEFORE YOU START IMPLEMENTING:
Ideally it should be lightweight (with no external dependencies). Please do an analisys of all pros and cons of this and let's discuss it first.

As a result, adding a new dialog should be an easy process (like adding a data attribute).

### Environment
Please use Sass and plain HTML with no dependencies on any responsive frameworks like Foundation.

Browser support:
IE 10+

### Acceptance criteria
- The desktop version of a dialog box should have a default maximum size (for ex. 600x400px) and be centered both verticaly and horizontaly.
- The mobile version should take the whole screen.
- The dialog box should have a close button in the upper right corner.
- When the dialog is opened, make sure the underlying content is not scrollable.
- Add some slick animation for open/close events.
- Pay a special attention for mobile version (test it in a mobile simulator - Xcode) - make sure there are no redundant scroll bars.
- It should be possible to add as many dialogs on a single page as needed.

...
