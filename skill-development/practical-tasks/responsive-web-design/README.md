# Responsive Web Design

## The goals:
- Understand main principles of responsive design, apply mobile-first approach to progressively enhance the styles. Identify key points of what makes a website responsive. Understand the differences between fixed, fluid and adaptive. Use basic testing environment to evaluate websites for responsiveness.

- Design and implement responsive page designs using media queries, semantic tags, and logical operators.

- Learn to build a Web app interface for mobile users and an alternative entry page for laptop, tablet and desktop users information interactively and display page content differently in different viewports

- Define images, text, and layout elements using relative units of measurement

- Learn to create alternative navigation menus for different viewports using media queries in conjunction with HTML lists

## Benefits of the course:

You will be able to identify and address every aspect of responsive Web design. Build a Web site that is intuitive, inviting, accessible, and attractive in every possible device and environment.

## Contents

* [ Grid ](grid/OVERVIEW.md)
* [ Navigation ](navigation/OVERVIEW.md)
* [ Popup ](popup/OVERVIEW.md)
* [ Typography ](typography/OVERVIEW.md)